.. include:: ../README.rst

Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   tree
   device
   api/index

:ref:`genindex`
