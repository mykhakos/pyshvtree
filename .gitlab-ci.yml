stages:
  - test
  - lint
  - style
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"

variables:
  GIT_DEPTH: 0
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_UPDATE_FLAGS: --jobs 2

.nix:
  image: nixos/nix
  tags:
    - docker
  cache:
    key: "nix"
    paths:
      - ".nix-cache"
  before_script:
    - echo "experimental-features = nix-command flakes" >>/etc/nix/nix.conf
    - echo "extra-substituters = file://$PWD/.nix-cache" >>/etc/nix/nix.conf
    - find /nix/store -maxdepth 1 ! -name \*.drv | sort >/nix/.before
  after_script:
    - find /nix/store -maxdepth 1 ! -name \*.drv | sort >/nix/.after
    - comm -13 /nix/.before /nix/.after | xargs nix copy --to "file://$PWD/.nix-cache"

## Test stage ##################################################################
tests:
  stage: test
  extends: .nix
  script:
    - nix develop -c pytest -v --junitxml=report.xml --cov=. --cov-report xml:coverage.xml --cov-report html:coverage --cov-report term
  artifacts:
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - 'coverage/'
  coverage: '/^TOTAL.*\s+([^\s]+)%$/'

nix-build:
  stage: test
  extends: .nix
  script:
    - nix build

nix-check:
  stage: test
  extends: .nix
  script:
    - nix flake check

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

## Linters #####################################################################
.lint:
  stage: lint
  extends: .nix
  needs: []

pylint:
  extends: .lint
  script:
    - nix develop -c pylint --output-format=colorized .

mypy:
  extends: .lint
  script:
    - mkdir .mypy-cache
    - nix develop -c mypy --cache-dir .mypy-cache --ignore-missing-imports .

## Style stage #################################################################
.style:
  stage: style
  extends: .nix
  needs: []
  allow_failure: true

black:
  extends: .style
  script:
    - nix develop -c black --diff --check .

isort:
  extends: .style
  script:
    - nix develop -c isort --diff --check .

pydocstyle:
  extends: .style
  script:
    - nix develop -c pydocstyle --match='(?!tests/)*.py' .

editorconfig-checker:
  extends: .style
  script:
    - nix develop -c editorconfig-checker -exclude '.nix-cache/.*'

gitlint:
  extends: .style
  script:
    - git fetch
    - nix develop -c gitlint --commits origin/master..$CI_COMMIT_SHA

## Release creation ############################################################
.deploy:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG'
  needs:
    - job: tests
      artifacts: false

release:
  extends: .deploy
  image: "registry.nic.cz/turris/python-template/baseimg:release"
  before_script:
    - "apk update"
    - "apk add bash curl jq py3-pip"
    - "pip install yq"
  script:
    - "bash .release.sh"

gitlab-pypi:
  extends: [.nix, .deploy]
  script:
    - nix develop -c python3 -m build
    - TWINE_PASSWORD=$CI_JOB_TOKEN TWINE_USERNAME=gitlab-ci-token nix develop -c twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

pages:
  stage: deploy
  extends: .nix
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  needs: []
  script:
    - git fetch
    - nix develop -c sphinx-multiversion docs public
    - | # Add index.html to public root to redirect to $CI_DEFUALT_BRANCH/index.html
      cat >public/index.html << EOF
      <!DOCTYPE html>
        <html>
          <head>
            <title>Redirecting to $CI_DEFAULT_BRANCH branch</title>
            <meta charset="utf-8">
            <meta http-equiv="refresh" content="0; url=./$CI_DEFAULT_BRANCH/index.html">
            <link rel="canonical" href="$CI_PAGES_URL/$CI_DEFAULT_BRANCH/index.html">
          </head>
        </html>
      EOF
  artifacts:
    paths:
    - public
